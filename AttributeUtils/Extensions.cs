﻿using System;
using System.Linq;
using System.Reflection;

namespace AttributeUtils
{
    public static class Extensions
    {
        public static Boolean HasAttribute<T>(this Enum enumValue) where T : Attribute
        {
            return enumValue.GetAttribute<T>() != null;
        }

        public static T GetAttribute<T>(this Enum enumValue) where T : Attribute
        {
            MemberInfo memberInfo = enumValue.GetType().GetMember(enumValue.ToString())
                .FirstOrDefault();

            return memberInfo?.GetCustomAttributes(typeof(T), false).FirstOrDefault() as T;
        }

        public static T GetAttribute<T>(this object obj) where T : Attribute
        {
            return obj.GetType().GetCustomAttribute<T>();
        }
    }
}
