﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace AttributeUtils
{
    public static class AttributeFetcher
    {
        public static TAttributeType GetClassAttribute<TClassType, TAttributeType>() where TAttributeType : Attribute
        {
            return typeof(TClassType).GetCustomAttribute<TAttributeType>();
        }

        public static TAttributeType GetPropertyAttribute<TClassType, TAttributeType>(Expression<Func<TClassType, object>> propertyExpression) where TAttributeType : Attribute
        {
            var expression = (MemberExpression)propertyExpression.Body;
            PropertyInfo propertyInfo = (PropertyInfo)expression.Member;
            return propertyInfo.GetCustomAttributes(typeof(TAttributeType), true).FirstOrDefault() as TAttributeType;
        }
    }
}
