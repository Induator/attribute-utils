**Usage:**


```
#!C#
AttributeFetcher.GetClassAttribute<TClassType, TAttributeType>();
AttributeFetcher.GetPropertyAttribute<TClassType, TAttributeType>(i => i.Property);

```